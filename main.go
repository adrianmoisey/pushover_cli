package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
)

func main() {
	token := os.Getenv("TOKEN")
	user := os.Getenv("USER")
	message := os.Getenv("MESSAGE")
	req, _ := http.PostForm("https://api.pushover.net/1/messages.json",
		url.Values{"token": {token}, "user": {user}, "message": {message}})
	fmt.Println(req.Status)
}
